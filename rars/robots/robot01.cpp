/**
 * Vorlage für den Robot im Software-Projekt
 *
 * @author    Ingo Haschler <lehre@ingohaschler.de>
 * @version   et12
 */

//--------------------------------------------------------------------------
//                           I N C L U D E
//--------------------------------------------------------------------------

#include "car.h"
#include <math.h>

//--------------------------------------------------------------------------
//                           Class Robot01
//--------------------------------------------------------------------------

class Robot01 : public Driver
{
public:
    // Konstruktor
    Robot01()
    {
        // Der Name des Robots
        m_sName = "Robot01";
        // Namen der Autoren
        m_sAuthor = "";
        // Hier die vorgegebenen Farben eintragen
        m_iNoseColor = oBLUE;
        m_iTailColor = oBLUE;
        m_sBitmapName2D = "car_blue_blue";
        // Für alle Gruppen gleich
        m_sModel3D = "futura";
    }
    double getspeed(double radius)     // returns maximum cornering speed, fps
    {
        double rad;                             // absolute value of the radius
        rad = radius < 0 ? -radius : radius;    // make radius positive
        return sqrt(rad * 32.2 * 0.95);     // compute the speed
    }



    con_vec drive(situation& s)
  {
    con_vec result = CON_VEC_EMPTY;

    if( s.starting )
    {
      result.fuel_amount = MAX_FUEL;     // fuel when starting
    }

    // service routine in the host software to handle getting unstuck from
    // from crashes and pileups:
    if(stuck(s.backward, s.v,s.vn, s.to_lft,s.to_rgt, &result.alpha,&result.vc))
        return result;

    double speed=50.0;
    double rad=0.0;
    if(s.nex_rad>0)
        rad=s.nex_rad;
    else
        rad=s.nex_rad*-1;


    //lange Gerade, bremsen noch nicht nötig
    if(s.cur_rad==0.0 && s.to_end>150 && (s.v-getspeed(rad)<50))
    {
        speed=s.vc*1.05;
    }
    //kurze Gerade
    else if(s.cur_rad==0.0 && s.to_end<=150 && (s.v-getspeed(rad)>30))
    {
        speed=getspeed(rad);
        //speed=s.v*0.95;
    }
    //Anfang der Kurve
    else if(s.cur_rad>0.0 && s.to_end>100)
    {
        speed=s.v;
    }
    //Ende der Kurve
    else if(s.cur_rad>0.0 && s.to_end<=100)
    {
        //speed=s.v * 1.02;
        speed=s.v;
    }

    result.vc = speed;
    result.alpha = 0.01*(s.to_lft-s.to_rgt)-0.01*s.vn;
    result.request_pit = 0;

    return result;
  }
};

/**
 * Diese Methode darf nicht verändert werden.
 * Sie wird vom Framework aufgerufen, um den Robot zu erzeugen.
 * Der Name leitet sich (wie der Klassenname) von der Gruppenbezeichnung ab.
 */
Driver * getRobot01Instance()
{
    return new Robot01();
}
